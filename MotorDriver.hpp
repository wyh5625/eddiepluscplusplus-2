/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotorDriver.hpp
 * Author: user
 *
 * Created on January 30, 2017, 1:33 PM
 */

#ifndef MOTORDRIVER_HPP
#define MOTORDRIVER_HPP
#include "GPIOClass.hpp"
#define GPIOS   5


class MotorDriver {
public:
    MotorDriver();
    MotorDriver(const MotorDriver& orig);
    virtual ~MotorDriver();
    int Enable();
    void Disable();

    void Standby(char p_option);

    void SetDirectionLeft(char p_direction);
    void SetDirectionRight(char p_direction);

    void SetSpeedLeft(float p_speed);
    void SetSpeedRight(float p_speed);

private:
  GPIOClass gpio1, gpio2; 
  GPIOClass gpio[GPIOS];
  int pwm0, pwm1, ain1, ain2, bin1, bin2;
  char c_system_command[128];
};

#endif /* MOTORDRIVER_HPP */

