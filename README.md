# Introduction #

This is a project to port the software of EddiePlus  http://www.thingiverse.com/thing:694969 balance robot in C++ 
with object oriented approach

The source code is a full NetBeans 8.2 project profile. To participate the development

1. Fork clone this repository to your BitBucket account.
2. Clone your forked repository to your computer.
3. Open it with  NetBeans 8.2, you may need change project proprieties.
4. Iteration to do -
    * Write your code
    * Test your code
    * Commit and push updates to your forked clone repository
5. Make a pull request on your repository

# Requirements #
To involve in the development, you need

* [NetBeans IDE](https://netbeans.org/)
* [Raspberry Pi](https://www.raspberrypi.org/)
* MPU-9250 single chip IMU
* [libmraa](https://github.com/intel-iot-devkit/mraa)