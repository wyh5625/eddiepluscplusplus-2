/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IMUClass.hpp
 * Author: user
 *
 * Created on January 30, 2017, 5:34 PM
 */

#ifndef IMUCLASS_HPP
#define IMUCLASS_HPP

#include <mraa.h>
#include <math.h>

#define MAX_BUFFER_LENGTH 512

class IMUClass {
public:
    IMUClass();
    IMUClass(const IMUClass& orig);
    void imuinit();
    void readSensors();
    void getOrientation();
   float i2cHeading, i2cPitch, i2cRoll;
      float gx, gy, gz;
    virtual ~IMUClass();
private:
    uint8_t rx_tx_buf[MAX_BUFFER_LENGTH];
    mraa_i2c_context i2c;
    float temp;
    float mx, my, mz;
    float ax, ay, az;
 
    void readMag();
    void readAccel();
    void readGyro();
    char readi2c(int address, int reg, int count);
    void sendi2c(unsigned int address, unsigned int reg, unsigned char tosend);

};

#endif /* IMUCLASS_HPP */

